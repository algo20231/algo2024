﻿using System;
using System.Diagnostics;
using System.IO;

class SortingTimeMeasurement
{
    static void Main(string[] args)
    {
        int[] sizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

        using (StreamWriter sw = new StreamWriter("sorting_times.csv"))
        {
            sw.WriteLine("Array Size,Shell Sort Time (ms),Heap Sort Time (ms),Counting Sort Time (ms)");

            foreach (int size in sizes)
            {
                int[] array = GenerateRandomArray(size);

                Stopwatch stopwatchShellSort = Stopwatch.StartNew();
                ShellSortAlgorithm(array);
                stopwatchShellSort.Stop();

                Stopwatch stopwatchHeapSort = Stopwatch.StartNew();
                HeapSortAlgorithm(array);
                stopwatchHeapSort.Stop();

                Stopwatch stopwatchCountingSort = Stopwatch.StartNew();
                CountingSortAlgorithm(array);
                stopwatchCountingSort.Stop();

                long elapsedTimeShellSort = stopwatchShellSort.ElapsedMilliseconds;
                long elapsedTimeHeapSort = stopwatchHeapSort.ElapsedMilliseconds;
                long elapsedTimeCountingSort = stopwatchCountingSort.ElapsedMilliseconds;

                Console.WriteLine($"Час сортування для розміру масиву {size}:");
                Console.WriteLine($"Сортування Шелла: {elapsedTimeShellSort} мс");
                Console.WriteLine($"Пірамідальне сортування: {elapsedTimeHeapSort} мс");
                Console.WriteLine($"Сортування підрахунком: {elapsedTimeCountingSort} мс");

                // Записуємо дані у файл CSV
                sw.WriteLine($"{size},{elapsedTimeShellSort},{elapsedTimeHeapSort},{elapsedTimeCountingSort}");
            }
        }

        Console.WriteLine("Результати вимірювань збережено у файл sorting_times.csv");
    }

    static int[] GenerateRandomArray(int size)
    {
        Random random = new Random();
        int[] array = new int[size];
        for (int i = 0; i < size; i++)
        {
            array[i] = random.Next();
        }
        return array;
    }

    // Реалізація сортування Шелла
    static void ShellSortAlgorithm(int[] array)
    {
        int n = array.Length;
        for (int gap = n / 2; gap > 0; gap /= 2)
        {
            for (int i = gap; i < n; i++)
            {
                int temp = array[i];
                int j;
                for (j = i; j >= gap && array[j - gap] > temp; j -= gap)
                    array[j] = array[j - gap];
                array[j] = temp;
            }
        }
    }

    // Реалізація пірамідального сортування
    static void HeapSortAlgorithm(int[] array)
    {
        int n = array.Length;

        // Побудова купи (heapify)
        for (int i = n / 2 - 1; i >= 0; i--)
            Heapify(array, n, i);

        // Витягання елементів з купи один за одним
        for (int i = n - 1; i >= 0; i--)
        {
            // Переміщаємо корінь у кінець масиву
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;

            // Викликаємо heapify на зменшеній купі
            Heapify(array, i, 0);
        }
    }

    static void Heapify(int[] array, int n, int i)
    {
        int largest = i; // Ініціалізуємо найбільший елемент як корінь
        int left = 2 * i + 1; // Індекс лівого дитячого вузла
        int right = 2 * i + 2; // Індекс правого дитячого вузла

        // Якщо лівий дитячий вузол більший за корінь
        if (left < n && array[left] > array[largest])
            largest = left;

        // Якщо правий дитячий вузол більший за найбільший в попередньому кроці
        if (right < n && array[right] > array[largest])
            largest = right;

        // Якщо найбільший елемент не є коренем
        if (largest != i)
        {
            int temp = array[i];
            array[i] = array[largest];
            array[largest] = temp;

            // Рекурсивно викликаємо heapify для піддерева
            Heapify(array, n, largest);
        }
    }

    // Реалізація сортування підрахунком
    static void CountingSortAlgorithm(int[] array)
    {
        int max = FindMaxValue(array);

        // Перевіряємо, чи max перевищує максимальне значення int
        if (max > int.MaxValue)
        {
            throw new InvalidOperationException("Максимальне значення в масиві перевищує допустимий діапазон для сортування підрахунком.");
        }

        // Обмежуємо max, якщо воно перевищує максимальне значення int
        if (max >= int.MaxValue)
        {
            max = int.MaxValue - 1;
        }

        int[] counts = new int[max];

        // Підрахунок кількості елементів
        foreach (int num in array)
            counts[num]++;

        // Відновлення відсортованого масиву
        int index = 0;
        for (int i = 0; i <= max; i++)
        {
            while (counts[i] > 0)
            {
                array[index] = i;
                index++;
                counts[i]--;
            }
        }
    }


    static int FindMaxValue(int[] array)
    {
        int max = int.MinValue;
        foreach (int num in array)
            if (num > max)
                max = num;
        return max;
    }
}
