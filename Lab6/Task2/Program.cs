﻿using System;

class ShellSort
{
    static void Main(string[] args)
    {
        // Задані значення таблиці
        int[] increments = { 175, 87, 41, 19, 9, 4, 1 };

        // Генеруємо випадковий масив
        float[] array = GenerateRandomArray(10);

        Console.WriteLine("Масив перед сортуванням:");
        PrintArray(array);

        // Сортуємо масив за допомогою сортування Шелла
        ShellSortAlgorithm(array, increments);

        Console.WriteLine("Масив після сортування:");
        PrintArray(array);
    }

    static float[] GenerateRandomArray(int size)
    {
        Random random = new Random();
        float[] array = new float[size];
        for (int i = 0; i < size; i++)
        {
            array[i] = (float)random.NextDouble() * 400; // Генеруємо випадкове число у діапазоні [0, 400]
        }
        return array;
    }

    static void ShellSortAlgorithm(float[] array, int[] increments)
    {
        foreach (int increment in increments)
        {
            for (int i = increment; i < array.Length; i++)
            {
                float temp = array[i];
                int j;
                for (j = i; j >= increment && array[j - increment] > temp; j -= increment)
                {
                    array[j] = array[j - increment];
                }
                array[j] = temp;
            }
        }
    }

    static void PrintArray(float[] array)
    {
        foreach (float num in array)
        {
            Console.Write(num + " ");
        }
        Console.WriteLine();
    }
}
