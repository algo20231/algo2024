﻿#include <stdio.h>
#include <windows.h>
// Функція для побудови піраміди
void heapify(double arr[], int n, int i) {
    int largest = i; // Ініціалізуємо найбільший елемент як корінь
    int l = 2 * i + 1; // Лівий дочірній елемент
    int r = 2 * i + 2; // Правий дочірній елемент

    // Перевірка, чи існує лівий дочірній елемент більший за корінь
    if (l < n && arr[l] > arr[largest])
        largest = l;

    // Перевірка, чи існує правий дочірній елемент більший за корінь
    if (r < n && arr[r] > arr[largest])
        largest = r;

    // Якщо найбільший елемент не корінь
    if (largest != i) {
        // Міняємо корінь з найбільшим елементом
        double temp = arr[i];
        arr[i] = arr[largest];
        arr[largest] = temp;

        // Рекурсивно викликаємо heapify() для зміненої піраміди
        heapify(arr, n, largest);
    }
}

// Функція для сортування масиву за допомогою піраміди
void heap_sort(double arr[], int n) {
    // Створення максимальної піраміди
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // Поступове видалення елементів з піраміди
    for (int i = n - 1; i > 0; i--) {
        // Міняємо корінь з останнім елементом
        double temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;

        // Викликаємо heapify() для зменшеної піраміди
        heapify(arr, i, 0);
    }
}

// Приклад використання
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    double arr[] = { -57.2, -29.3, 8.5, -100, -10, 0, 5.4, 10 };
    int n = sizeof(arr) / sizeof(arr[0]);

    printf("Не відсортований масив: \n");
    for (int i = 0; i < n; ++i)
        printf("%f ", arr[i]);
    printf("\n");

    heap_sort(arr, n);

    printf("Відсортований масив: \n");
    for (int i = 0; i < n; ++i)
        printf("%f ", arr[i]);
    printf("\n");
    return 0;
}
