﻿using System;

class CountingSort
{
    static void Main(string[] args)
    {
        int[] array = { 4, 2, 2, 8, 3, 3, 1 };

        Console.WriteLine("Масив перед сортуванням:");
        PrintArray(array);

        // Сортування підрахунком
        CountingSortAlgorithm(array);

        Console.WriteLine("Масив після сортування:");
        PrintArray(array);
    }

    static void CountingSortAlgorithm(int[] array)
    {
        int max = FindMaxValue(array);
        int[] counts = new int[max + 1];

        // Підрахунок кількості елементів
        foreach (int num in array)
        {
            counts[num]++;
        }

        // Відновлення відсортованого масиву
        int index = 0;
        for (int i = 0; i <= max; i++)
        {
            while (counts[i] > 0)
            {
                array[index] = i;
                index++;
                counts[i]--;
            }
        }
    }

    static int FindMaxValue(int[] array)
    {
        int max = int.MinValue;
        foreach (int num in array)
        {
            if (num > max)
            {
                max = num;
            }
        }
        return max;
    }

    static void PrintArray(int[] array)
    {
        foreach (int num in array)
        {
            Console.Write(num + " ");
        }
        Console.WriteLine();
    }
}
