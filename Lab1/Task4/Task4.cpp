﻿#include <stdio.h>
struct FloatBits {
    unsigned int bit1 : 1;
    unsigned int bit2 : 1;
    unsigned int bit3 : 1;
    unsigned int bit4 : 1;
    unsigned int bit5 : 1;
    unsigned int bit6 : 1;
    unsigned int bit7 : 1;
    unsigned int bit8 : 1;
    unsigned int bit9 : 1;
    unsigned int bit10 : 1;
    unsigned int bit11 : 1;
    unsigned int bit12 : 1;
    unsigned int bit13 : 1;
    unsigned int bit14 : 1;
    unsigned int bit15 : 1;
    unsigned int bit16 : 1;
    unsigned int bit17 : 1;
    unsigned int bit18 : 1;
    unsigned int bit19 : 1;
    unsigned int bit20 : 1;
    unsigned int bit21 : 1;
    unsigned int bit22 : 1;
    unsigned int bit23 : 1;
    unsigned int bit24 : 1;
    unsigned int bit25 : 1;
    unsigned int bit26 : 1;
    unsigned int bit27 : 1;
    unsigned int bit28 : 1;
    unsigned int bit29 : 1;
    unsigned int bit30 : 1;
    unsigned int bit31 : 1;
    unsigned int bit32 : 1;
};

// Structure to represent a number in terms of individual bytes
struct FloatBytes {
    unsigned int byte1 : 8;
    unsigned int byte2 : 8;
    unsigned int byte3 : 8;
    unsigned int byte4 : 8;
};

// Union that combines the above structures to represent a float number
union FloatingPoint {
    float num;
    struct FloatBits bits;
    struct FloatBytes bytes;
};

int main() {

    // Create an object of type union FloatingPoint
    union FloatingPoint fp;

    // Ask the user to enter a float number
    printf("Enter a number: ");
    scanf_s("%f", &fp.num);

    // Display the bits of the number in binary format
    printf("Bitwise representation of the number:\n");
    printf("%d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d\n",
        fp.bits.bit32, fp.bits.bit31, fp.bits.bit30, fp.bits.bit29, fp.bits.bit28, fp.bits.bit27, fp.bits.bit26, fp.bits.bit25,
        fp.bits.bit24, fp.bits.bit23, fp.bits.bit22, fp.bits.bit21, fp.bits.bit20, fp.bits.bit19, fp.bits.bit18, fp.bits.bit17,
        fp.bits.bit16, fp.bits.bit15, fp.bits.bit14, fp.bits.bit13, fp.bits.bit12, fp.bits.bit11, fp.bits.bit10, fp.bits.bit9,
        fp.bits.bit8, fp.bits.bit7, fp.bits.bit6, fp.bits.bit5, fp.bits.bit4, fp.bits.bit3, fp.bits.bit2, fp.bits.bit1);

    // Display the bytes of the number in hexadecimal format
    printf("Byte-wise representation of the number:\n%x %x %x %x\n", fp.bytes.byte4, fp.bytes.byte3, fp.bytes.byte2, fp.bytes.byte1);

    // Display the mantissa of the number
    printf("Mantissa:\n%d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d\n",
        fp.bits.bit23, fp.bits.bit22, fp.bits.bit21, fp.bits.bit20, fp.bits.bit19, fp.bits.bit18, fp.bits.bit17, fp.bits.bit16,
        fp.bits.bit15, fp.bits.bit14, fp.bits.bit13, fp.bits.bit12, fp.bits.bit11, fp.bits.bit10, fp.bits.bit9, fp.bits.bit8,
        fp.bits.bit7, fp.bits.bit6, fp.bits.bit5, fp.bits.bit4, fp.bits.bit3, fp.bits.bit2, fp.bits.bit1);

    // Display the exponent of the number
    printf("Exponent:\n%d%d%d%d %d%d%d%d\n",
        fp.bits.bit31, fp.bits.bit30, fp.bits.bit29, fp.bits.bit28, fp.bits.bit27, fp.bits.bit26, fp.bits.bit25, fp.bits.bit24);

    // Display the sign of the number
    printf("Sign: %d\n", fp.bits.bit32);

    // Display the size of the union FloatingPoint structure
    printf("Size of the structure: %d\n", sizeof(union FloatingPoint));

    return 0;
}
