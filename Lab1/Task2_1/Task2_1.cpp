﻿#include <stdio.h>
#include <Windows.h>

struct IntWithSign {
    signed short value;
    unsigned short sign : 1;
};

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    struct IntWithSign num;

    printf("Введіть ціле число типу signed short: ");
    scanf_s("%hd", &num.value);

    if (num.value < 0) {
        num.sign = 1;
        num.value = -num.value;
    }
    else {
        num.sign = 0;
    }

    printf("Введене число: %d, Знак: %s\n", num.value, (num.sign == 1) ? "від'ємне" : "додатнє");

    return 0;
}
