﻿#include <stdio.h>
#include <Windows.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    signed char result_a, result_b, result_c, result_g, result_d, result_e;
    signed char constant_1 = 5;
    signed char constant_2 = 127;
    signed char constant_3 = -120;
    signed char constant_4 = -5;
    signed char constant_5 = 56;
    signed char constant_6 = 38;

    result_a = constant_1 + constant_2;
    result_b = constant_1 - constant_2;
    result_c = constant_3 - constant_6;
    result_g = (signed char)(unsigned char)constant_4;
    result_d = constant_5 & constant_6;
    result_e = constant_5 | constant_6;

    printf("Результат a): %d\n", result_a);
    printf("Результат b): %d\n", result_b);
    printf("Результат в): %d\n", result_c);
    printf("Результат г): %d\n", result_g);
    printf("Результат д): %d\n", result_d);
    printf("Результат е): %d\n", result_e);

    return 0;
}
