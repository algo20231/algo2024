﻿#include <stdio.h>
#include <time.h>

struct CompactDateTime {
    unsigned int year : 5;
    unsigned int month : 4;
    unsigned int day : 5;
    unsigned int hour : 5;
    unsigned int minute : 6;
    unsigned int second : 6;
};

void printCompactDateTime(struct CompactDateTime dt) {
    printf("%02d-%02d-%d %02d:%02d:%02d\n", dt.day, dt.month, dt.year + 1995, dt.hour, dt.minute, dt.second);
}

int main() {
    time_t currentTime;
    struct tm localTime;
    struct CompactDateTime compactDateTime;

    time(&currentTime);
    localtime_s(&localTime, &currentTime);


    compactDateTime.year = localTime.tm_year - 95;
    compactDateTime.month = localTime.tm_mon + 1;
    compactDateTime.day = localTime.tm_mday;
    compactDateTime.hour = localTime.tm_hour;
    compactDateTime.minute = localTime.tm_min;
    compactDateTime.second = localTime.tm_sec;

    printf("Compact Date and Time: ");
    printCompactDateTime(compactDateTime);

    printf("Size of CompactDateTime: %zu bytes\n", sizeof(compactDateTime));

    struct tm stdTime;
    printf("Size of struct tm: %zu bytes\n", sizeof(stdTime));

    return 0;
}
