﻿#include <stdio.h>
#include <Windows.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    signed short value;

    printf("Введіть ціле число типу signed short: ");
    scanf_s("%hd", &value);

    unsigned short sign = (value < 0) ? 1 : 0;

    printf("Введене число: %hd, Знак: %s\n", (value < 0) ? -value : value, (sign == 1) ? "від'ємне" : "додатнє");

    return 0;
}
