﻿#include <stdio.h>

// Рекурсивна функція для обчислення числа Фібоначчі
int Fibonacci(int n) {
    if (n < 0) {
        printf("n повинно бути невід'ємним цілим числом.\n");
        return -1; // Повертаємо -1 для позначення помилки
    }
    if (n == 0) {
        return 0;
    }
    if (n == 1) {
        return 1;
    }
    return Fibonacci(n - 1) + Fibonacci(n - 2);
}

int main() {
    // Тестові значення
    for (int i = 0; i <= 10; i++) {
        printf("F(%d) = %d\n", i, Fibonacci(i));
    }

    return 0;
}
