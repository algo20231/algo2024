﻿#include <stdio.h> 
#include <stdbool.h> 
#include <string.h>
#include<windows.h>
#include <stdlib.h> 

#define MAX_VERTICES 19 // Константа, що задає максимальну кількість вершин (у нашому випадку міст)

void DepthFirstSearch(int edges[MAX_VERTICES][MAX_VERTICES], int numVertices, bool visited[MAX_VERTICES], int startVertex, const char* cityNames[MAX_VERTICES]);
// Прототип функції для глибокого пошуку (DFS)

void BreadthFirstSearch(int edges[MAX_VERTICES][MAX_VERTICES], int numVertices, bool visited[MAX_VERTICES], int startVertex, int km[MAX_VERTICES], const char* cityNames[MAX_VERTICES]);
// Прототип функції для широкого пошуку (BFS)

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    const char* cityNames[MAX_VERTICES] =
    {
        "Київ", "Житомир", "Новоград-Волинський", "Рівне", "Луцьк",// Оголошення та ініціалізація масиву назв міст
        "Бердичів", "Вінниця", "Хмельницький", "Тернопіль",
        "Шепетівка", "Біла Церква", "Умань","Черкаси",
        "Кременчук", "Полтава", "Харків","Прилуки", "Суми", "Миргород"
    };

    // Оголосив та ініціалізував масив відстаней між містами
    int km[MAX_VERTICES] = { 135, 80, 100, 68, 38, 73, 110, 104, 115, 78, 115, 146, 105, 181, 130, 128, 175, 109, 0 };

    // Оголосив та частково ініціалізував матриці суміжності для представлення графу
    int edges[MAX_VERTICES][MAX_VERTICES] =
    {
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        // Додав решту рядків для заповнення матриці adjacency
    };

    // Оголошення та ініціалізація масиву для відстеження відвіданих міст
    bool visited[MAX_VERTICES] = { false };

    printf("Павлюк Олександр ІПЗ 23-2\nЛабораторна робота №7-8\nDFS\n");
    // Виведення інформації про мене

    // Глибокий пошук для всіх міст, які ще не були відвідані
    for (int i = 0; i < MAX_VERTICES; i++)
    {
        if (!visited[i])
            DepthFirstSearch(edges, MAX_VERTICES, visited, i, cityNames);
    }

    // Скидання масиву visited для подальшого використання в BFS
    memset(visited, 0, sizeof(visited));

    printf("\nBFS\n");
    // Широкий пошук для всіх міст, які ще не були відвідані
    for (int i = 0; i < MAX_VERTICES; i++)
    {
        if (!visited[i])
            BreadthFirstSearch(edges, MAX_VERTICES, visited, i, km, cityNames);
    }

    // Виведення всіх можливих маршрутів і відстаней між містами
    printf("\nВсі можливі маршрути:\n");
    printf("\nКиїв - Житомир: %d\n", km[0]);
    printf("Київ - Житомир - Новоград-Волинський: %d\n", km[0] + km[1]);
    printf("Київ - Житомир - Новоград-Волинський - Рівне: %d\n", km[0] + km[1] + km[2]);
    printf("Київ - Житомир - Новоград-Волинський - Рівне - Луцьк: %d\n", km[0] + km[1] + km[2] + km[3]);
    printf("Київ - Житомир - Бердичів: %d\n", km[0] + km[4]);
    printf("Київ - Житомир - Бердичів - Вінниця: %d\n", km[0] + km[4] + km[5]);
    printf("Київ - Житомир - Бердичів - Вінниця - Хмельницький: %d\n", km[0] + km[4] + km[5] + km[6]);
    printf("Київ - Житомир - Бердичів - Вінниця - Хмельницький - Тернопіль: %d\n", km[0] + km[4] + km[5] + km[6] + km[7]);
    printf("Київ - Житомир - Шепетівка: %d\n", km[0] + km[10]);
    printf("Київ - Біла Церква - Умань: %d\n", km[9] + km[10]);
    printf("Київ - Біла Церква - Черкаси - Кременчук: %d\n", km[9] + km[11] + km[12]);
    printf("Київ - Біла Церква - Полтава - Харків: %d\n", km[9] + km[13] + km[14]);
    printf("Київ - Прилуки - Суми: %d\n", km[15] + km[16]);
    printf("Київ - Прилуки - Миргород: %d\n", km[15] + km[17]);

    return 0;
}

// Функція для глибокого пошуку (DFS) у графі
void DepthFirstSearch(int edges[MAX_VERTICES][MAX_VERTICES], int numVertices, bool visited[MAX_VERTICES], int startVertex, const char* cityNames[MAX_VERTICES])
{
    visited[startVertex] = true; // Відзначаємо місто як відвідане
    printf("%s ", cityNames[startVertex]); // Виведення назви відвіданого міста


    // Проходження всіх сусідніх вершин
    for (int i = 0; i < numVertices; i++)
    {
        // Якщо місто ще не відвідане і є ребро (зв'язок) між містами
        if (!visited[i] && edges[startVertex][i] == 1)
        {
            DepthFirstSearch(edges, numVertices, visited, i, cityNames); // Рекурсивний виклик для відвідання наступного міста
        }
    }
}

// Функція для широкого пошуку (BFS) у графі
void BreadthFirstSearch(int edges[MAX_VERTICES][MAX_VERTICES], int numVertices, bool visited[MAX_VERTICES], int startVertex, int km[MAX_VERTICES], const char* cityNames[MAX_VERTICES])
{
    int queue[MAX_VERTICES]; // Оголошення черги
    int front = 0; // Індекс початку черги
    int rear = 0; // Індекс кінця черги

    queue[rear++] = startVertex; // Додавання початкової вершини в чергу
    visited[startVertex] = true; // Відзначаємо місто як відвідане

    // Проходження черги до її спорожнення
    while (front != rear)
    {
        int currentVertex = queue[front++]; // Витягуємо поточну вершину з черги
        printf("%s ", cityNames[currentVertex]); // Виведення назви відвіданого міста

        // Проходження всіх сусідніх вершин
        for (int i = 0; i < numVertices; i++)
        {
            // Якщо місто ще не відвідане і є ребро (зв'язок) між містами
            if (!visited[i] && edges[currentVertex][i] == 1)
            {
                queue[rear++] = i; // Додаємо місто в чергу
                visited[i] = true; // Відзначаємо місто як відвідане
            }
        }
    }
}
