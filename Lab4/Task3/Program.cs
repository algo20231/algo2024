﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task33
{
    internal class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
 System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.WriteLine("Вводьте вираз оберненого польського запису з символами через пробіл");


            string zapis = Console.ReadLine();
            string[] operators = zapis.Split(' ');
            double[] digits = new double[1];

            for (int i = 0; i < operators.Length; i++)
            {
                int length = digits.Length;
                if (double.TryParse(operators[i], out digits[length - 1]))
                {
                    Array.Resize(ref digits, length + 1);
                }
                else
                {
                    switch (operators[i])
                    {
                        case "+":
                            digits[length - 3] += digits[length - 2];
                            Array.Resize(ref digits, length - 1);
                            break;
                        case "-":
                            digits[length - 3] -= digits[length - 2];
                            Array.Resize(ref digits, length - 1);
                            break;
                        case "*":
                            digits[length - 3] *= digits[length - 2];
                            Array.Resize(ref digits, length - 1);
                            break;
                        case "/":
                            digits[length - 3] /= digits[length - 2];
                            Array.Resize(ref digits, length - 1);
                            break;
                        case "^":
                            digits[length - 3] = Math.Pow(digits[length - 3], digits[length - 2]);
                            Array.Resize(ref digits, length - 1);
                            break;
                        case "sqrt":
                            digits[length - 2] = Math.Sqrt(digits[length - 2]);
                            break;
                    }
                }
            }
            Console.WriteLine(digits[0]);
            Console.ReadLine();
        }
    }
}