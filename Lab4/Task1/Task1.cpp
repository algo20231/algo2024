﻿
//Вилучення елементу з початку
int popFront(cList* list, elemtype* data) {
	cNode* node;
	if (isEmptyList(list)) {
		return(-2);
	}
	node = list->head;
	list->head = list->head->next;
	if (!isEmptyList(list)) {
		list->head->prev = NULL;
	}
	else {
		list->tail = NULL;
	}
	*data = node->value;
	list->size--;
	free(node);
	return(0);
}
//Вставка елементу на початок
int pushFront(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-1);
	}
	node->value = *data;
	node->next = list->head;
	node->prev = NULL;
	if (!isEmptyList(list)) {
		list->head->prev = node;
	}
	else {
		list->tail = node;
	}
	list->head = node;
	list->size++;
	return(0);
}
//Вставка елементу в кінець
int pushBack(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-3);
	}
	node->value = *data;
	node->next = NULL;
	node->prev = list->tail;
	if (!isEmptyList(list)) {
		list->tail->next = node;
	}
	else {
		list->head = node;
	}
	list->tail = node;
	list->size++;
	return(0);
}
//Вилучення елемента з кінця
int popBack(cList* list, elemtype* data) {
	cNode* node = NULL;
	if (isEmptyList(list)) {
		return(-4);
	}
	node = list->tail;
	list->tail = list->tail->prev;
	if (!isEmptyList(list)) {
		list->tail->next = NULL;
	}
	else {
		list->head = NULL;
	}
	*data = node->value;
	list->size--;
	free(node);
	return(0);
}
//Пошук по індексу
cNode* getNode(cList* list, int index) {
	cNode* node = NULL;
	int i;
	if (index >= list->size) {
		return (NULL);
	}
	if (index < list->size / 2) {
		i = 0;
		node = list->head;
		while (node && i < index) {
			node = node->next;
			i++;
		}
	}

	else {
		i = list->size - 1;
		node = list->tail;
		while (node && i > index) {
			node = node->prev;
			i--;
		}
	}
	return node;
}

//Вставка елемента в середину
int insert(cList* list, int index, elemtype* value) {
	cNode* elm = NULL;
	cNode* ins = NULL;
	elm = getNode(list, index);
	if (elm == NULL) {
		return (-5);
	}
	ins = (cNode*)malloc(sizeof(cNode));
	ins->value = *value;
	ins->prev = elm;
	ins->next = elm->next;
	if (elm->next) {
		elm->next->prev = ins;
	}
	elm->next = ins;
	if (!elm->prev) {
		list->head = elm;
	}
	if (!elm->next) {
		list->tail = elm;
	}
	list->size++;
	return 0;
}
//Видалення елемента з середини
int deleteNode(cList* list, int index, elemtype* data) {
	cNode* elm = NULL;
	elemtype tmp = NULL;
	elm = getNode(list, index);
	if (elm == NULL) {
		return (-6);
	}
	if (elm->prev) {
		elm->prev->next = elm->next;
	}
	if (elm->next) {
		elm->next->prev = elm->prev;
	}
	tmp = elm->value;
	if (!elm->prev) {
		list->head = elm->next;
	}

	if (!elm->next) {
		list->tail = elm->prev;
	}
	*data = elm->value;
	free(elm);
	list->size--;
	return 0;
}
//Створення списку
cList* createList() {
	cList* list = (cList*)malloc(sizeof(cList));
	if (list) {
		list->size = 0;
		list->head = list->tail = NULL;
	}
	return list;
}
//Видалення списку
void deleteList(cList* list) {
	cNode* head = list->head;
	cNode* next = NULL;
	while (head) {
		next = head->next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}
