#ifndef LIST_H
#define LIST_H

// ��� ��������
typedef int elemtype;

// ���� ������
typedef struct cNode {
    elemtype value;
    struct cNode* next;
    struct cNode* prev;
} cNode;

// ������
typedef struct cList {
    cNode* head;
    cNode* tail;
    int size;
} cList;

// ��������� �������
cList* createList();
int isEmptyList(cList* list);
int pushFront(cList* list, elemtype* data);
int pushBack(cList* list, elemtype* data);
int popFront(cList* list, elemtype* data);
int popBack(cList* list, elemtype* data);
int insert(cList* list, int index, elemtype* value);
int deleteNode(cList* list, int index, elemtype* data);
cNode* getNode(cList* list, int index);
void deleteList(cList* list);

#endif // LIST_H
