﻿#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "list.h"  // Подключение заголовочного файла

void printList(cList* list, void (*func)(elemtype*)) {
    cNode* node = list->head;
    if (isEmptyList(list)) {
        return;
    }
    while (node) {
        func(&node->value);
        node = node->next;
    }
}

void printNode(elemtype* value) {
    printf("%d\n", *value);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    cList* mylist = createList();
    elemtype tmp;
    int task = 0;
    int index;

    do {
        printf("Що потрібно зробить?\n");
        printf("1) Додати елемент на початок списку\n");
        printf("2) Додати елемент на кінець списку\n");
        printf("3) Додати елемент після вказаного індексу\n");
        printf("4) Видалити перший елемент списку\n");
        printf("5) Видалити останній елемент списку\n");
        printf("6) Видалити елемент з вказаним індексом\n");
        printf("7) Введіть окремий елемент\n");
        printf("8) Вивести весь список\n");
        printf("9) Видалити список\n");
        printf("0) Вийти\n");
        scanf_s("%d", &task);

        elemtype elem;
        switch (task) {
        case 1:
            printf("\nВведіть значення елемента\n");
            scanf_s("%d", &elem);
            pushFront(mylist, &elem);
            printf("\n");
            break;
        case 2:
            printf("\nВведіть значення елемента\n");
            scanf_s("%d", &elem);
            pushBack(mylist, &elem);
            printf("\n");
            break;
        case 3:
            printf("\nВведіть значення елемента\n");
            scanf_s("%d", &elem);
            printf("Введіть індекс, після якого вставити елемент\n");
            scanf_s("%d", &index);
            insert(mylist, index, &elem);
            printf("\n");
            break;
        case 4:
            printf("\n");
            popFront(mylist, &tmp);
            printf("Елемент видалений\n");
            break;
        case 5:
            printf("\n");
            popBack(mylist, &tmp);
            printf("Елемент видалений\n");
            break;
        case 6:
            printf("\nВведіть індекс, який потрібно видалити\n");
            scanf_s("%d", &index);
            deleteNode(mylist, index, &tmp);
            printf("\n");
            break;
        case 7:
            printf("\nВведіть індекс, який потрібно вивести\n");
            scanf_s("%d", &index);
            printNode(&getNode(mylist, index)->value);
            printf("\n");
            break;
        case 8:
            printf("\n");
            printList(mylist, printNode);
            printf("\n");
            break;
        case 9:
            deleteList(mylist);
            break;
        }
        system("pause");
        system("cls");
    } while (task);

    return 0;
}
