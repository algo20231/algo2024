#include <stdio.h>
#include <stdlib.h>
#include "list.h"

// ��������, ���� �� ������
int isEmptyList(cList* list) {
    return list->head == NULL;
}

// �������� ������ ������
cList* createList() {
    cList* list = (cList*)malloc(sizeof(cList));
    if (list) {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}

// ������� �������� �� ������ ������
int pushFront(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return -1;
    }
    node->value = *data;
    node->next = list->head;
    node->prev = NULL;
    if (!isEmptyList(list)) {
        list->head->prev = node;
    }
    else {
        list->tail = node;
    }
    list->head = node;
    list->size++;
    return 0;
}

// ������� �������� � ����� ������
int pushBack(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return -1;
    }
    node->value = *data;
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list)) {
        list->tail->next = node;
    }
    else {
        list->head = node;
    }
    list->tail = node;
    list->size++;
    return 0;
}

// �������� �������� � ������ ������
int popFront(cList* list, elemtype* data) {
    if (isEmptyList(list)) {
        return -1;
    }
    cNode* node = list->head;
    list->head = list->head->next;
    if (list->head) {
        list->head->prev = NULL;
    }
    else {
        list->tail = NULL;
    }
    *data = node->value;
    free(node);
    list->size--;
    return 0;
}

// �������� �������� � ����� ������
int popBack(cList* list, elemtype* data) {
    if (isEmptyList(list)) {
        return -1;
    }
    cNode* node = list->tail;
    list->tail = list->tail->prev;
    if (list->tail) {
        list->tail->next = NULL;
    }
    else {
        list->head = NULL;
    }
    *data = node->value;
    free(node);
    list->size--;
    return 0;
}

// ��������� ���� �� �������
cNode* getNode(cList* list, int index) {
    if (index >= list->size || index < 0) {
        return NULL;
    }
    cNode* node = NULL;
    int i;
    if (index < list->size / 2) {
        node = list->head;
        for (i = 0; node && i < index; ++i) {
            node = node->next;
        }
    }
    else {
        node = list->tail;
        for (i = list->size - 1; node && i > index; --i) {
            node = node->prev;
        }
    }
    return node;
}

// ������� �������� � �������� ������
int insert(cList* list, int index, elemtype* value) {
    cNode* elm = getNode(list, index);
    if (!elm) {
        return -1;
    }
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return -1;
    }
    node->value = *value;
    node->next = elm->next;
    node->prev = elm;
    if (elm->next) {
        elm->next->prev = node;
    }
    elm->next = node;
    if (elm == list->tail) {
        list->tail = node;
    }
    list->size++;
    return 0;
}

// �������� �������� �� �������
int deleteNode(cList* list, int index, elemtype* data) {
    cNode* node = getNode(list, index);
    if (!node) {
        return -1;
    }
    if (node->prev) {
        node->prev->next = node->next;
    }
    else {
        list->head = node->next;
    }
    if (node->next) {
        node->next->prev = node->prev;
    }
    else {
        list->tail = node->prev;
    }
    *data = node->value;
    free(node);
    list->size--;
    return 0;
}

// �������� ������
void deleteList(cList* list) {
    cNode* current = list->head;
    cNode* next;
    while (current) {
        next = current->next;
        free(current);
        current = next;
    }
    free(list);
}
